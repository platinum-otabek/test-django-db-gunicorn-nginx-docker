#!/bin/sh

sleep 1

cd project
# run Celery worker for our project myproject with Celery configuration stored in Celeryconf
su -m myuser -c "celery -A config beat"
