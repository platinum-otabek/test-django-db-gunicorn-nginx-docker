#!/bin/sh

# wait for RabbitMQ server to start
sleep 1

cd project
# run Celery worker for our project myproject with Celery configuration stored in Celeryconf
su -m myuser -c "celery -A config.celery worker -l info --autoscale=5,1"
