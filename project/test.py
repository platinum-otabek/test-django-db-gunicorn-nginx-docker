import csv

with open('list_website.csv') as f:
    pamreader = csv.reader(f, delimiter=' ', quotechar='|')
    for row in pamreader:
        each = ', '.join(row)
        begin_www = each.find('www')
        if begin_www:
            finish_www = each.find('.uz')
            print(f'{each[:begin_www]} -> {each[begin_www:]}')
