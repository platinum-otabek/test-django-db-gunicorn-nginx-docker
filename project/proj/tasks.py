import json
import os.path
from datetime import datetime
from time import sleep

from celery.schedules import crontab

from config.celery import app

from proj.models import WebSiteModel
from proj.serializers import WebSiteSerializer, StatisticSerializer
import requests

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


@app.task
def update():
    all_web_sites = WebSiteModel.objects.all()
    serializer_data = WebSiteSerializer(all_web_sites, many=True)
    for i in serializer_data.data:

        try:
            r = requests.get(i['url'])
            r.raise_for_status()
            statisc = {
                'website': i['id'],
                'status': 1,
                'created_time': datetime.now()
            }
            # return f'{r.status_code} {i["name"]}'
            # print('try', r.status_code)
        except Exception as err:
            print('except', err)
            statisc = {
                'website': i['id'],
                'status': 0,
                'created_time': datetime.now()
            }
        data = StatisticSerializer(data=statisc)
        if data.is_valid():
            data.save()


app.conf.beat_schedule = {
    'every 1 min': {
        'task': 'proj.tasks.update',
        'schedule': crontab('*/1')
    }
}
