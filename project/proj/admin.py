from django.contrib import admin

# Register your models here.
from .models import WebSiteModel, StatisticsModel


class WebsiteAdmin(admin.ModelAdmin):
    pass


class StatisticsAdmin(admin.ModelAdmin):
    pass


admin.site.register(WebSiteModel, WebsiteAdmin)
admin.site.register(StatisticsModel, StatisticsAdmin)
