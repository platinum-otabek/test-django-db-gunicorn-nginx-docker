from datetime import datetime

from django.db import models


# Create your models here.

class WebSiteModel(models.Model):
    name = models.CharField(max_length=63, default='')
    url = models.URLField()

    class Meta:
        db_table = 'website'
        verbose_name = 'website'
        verbose_name_plural = 'websites'

    def __str__(self):
        return self.name


class StatisticsModel(models.Model):
    website = models.ForeignKey(WebSiteModel, null=True, on_delete=models.SET_NULL)
    status = models.BooleanField(default=False)
    created_time = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = 'statistic'
        ordering = ['-created_time']

    def __str__(self):
        return f'{self.website} -> {self.created_time}'
