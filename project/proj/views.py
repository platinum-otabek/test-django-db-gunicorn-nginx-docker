from django.shortcuts import render
from rest_framework import generics
# Create your views here.
from proj.models import WebSiteModel
from proj.serializers import WebSiteSerializer
from rest_framework.permissions import AllowAny

from proj.models import StatisticsModel
from proj.serializers import StatisticSerializer


class CreateWebsiteApiView(generics.ListCreateAPIView):
    queryset = WebSiteModel.objects.all()
    serializer_class = WebSiteSerializer
    permission_classes = [AllowAny]


class AllStatisticsApiView(generics.ListAPIView):
    queryset = StatisticsModel.objects.all()
    serializer_class = StatisticSerializer
    permission_classes = [AllowAny]


