from django.urls import path

from proj.views import CreateWebsiteApiView,AllStatisticsApiView

urlpatterns = [
    path('', CreateWebsiteApiView.as_view()),
    path('statistic', AllStatisticsApiView.as_view())
]
