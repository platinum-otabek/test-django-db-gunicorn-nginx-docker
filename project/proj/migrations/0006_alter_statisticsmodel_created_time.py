# Generated by Django 3.2.13 on 2022-05-27 11:59

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proj', '0005_alter_statisticsmodel_created_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statisticsmodel',
            name='created_time',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
