from rest_framework import serializers

from proj.models import WebSiteModel, StatisticsModel


class WebSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = WebSiteModel
        fields = ['id', 'name', 'url']


class StatisticSerializer(serializers.ModelSerializer):
    website = serializers.SerializerMethodField()
    class Meta:
        model = StatisticsModel
        fields = ['id', 'status', 'created_time', 'website']


    def get_website(self, obj):
        return obj.website.name
