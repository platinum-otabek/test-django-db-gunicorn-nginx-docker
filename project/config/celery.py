import os
from celery import Celery
from django.conf import settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
# BROKER_URL = 'pyamqp://admin:mypass@rabbitmq:5672//'
BROKER_URL = 'pyamqp://localhost@rabbitmq:5672//'

app = Celery('config')

app.config_from_object('django.conf:settings')

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
