FROM python:3.8.5-alpine

ADD requirements.txt /app/requirements.txt

RUN pip install --upgrade pip

WORKDIR /app

# Add requirements.txt to the image
RUN pip install -r requirements.txt

COPY ./project /app

RUN adduser --disabled-password --gecos '' myuser

#COPY ./entrypoint.sh /
#ENTRYPOINT ["sh", "/entrypoint.sh"]

