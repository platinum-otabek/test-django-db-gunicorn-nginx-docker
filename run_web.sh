#!/bin/sh

# wait for PSQL server to start
sleep 1

cd project
# prepare init migration
su -m myuser -c "python manage.py makemigrations"
# migrate db, so we have the latest db schema
su -m myuser -c "python manage.py migrate"

su -m myuser -c "python manage.py collectstatic --yes-input"

su -m myuser -c "gunicorn config.wsgi:application --bind 0.0.0.0:8000"




